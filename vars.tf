variable "environment" {
  description = "environment where this resource is used"
  type        = string
}

variable "bucket_name" {
  description = "name of the S3 bucket"
  type        = string
}

variable "bucket_acl" {
  description = "s3 bucket acl"
  type        = string
  default     = "private"
}

variable "bucket_versioning" {
  description = "activates bucket versioning"
  type        = bool
  default     = false
}

variable "bucket_policies" {
  description = "policies to attach to the bucket"
  type        = set(string)
  default     = []
}

variable "force_destroy" {
  description = "force destroy of S3 bucket, set to true to prepare a deletion"
  type        = bool
  default     = false
}

variable "objects" {
  description = "list of objects to upload to the bucket"
  type        = map(object({
    document_name = string,
    source        = string,
    content_type  = string,
  }))
  default = {}
}