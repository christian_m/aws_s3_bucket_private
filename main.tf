resource "aws_s3_bucket" "default" {
  bucket = var.bucket_name
  acl    = var.bucket_acl

  force_destroy = var.force_destroy

  lifecycle {
    prevent_destroy = false
  }

  versioning {
    enabled = var.bucket_versioning
  }

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }

  tags = {
    env = var.environment
  }
}

resource "aws_s3_bucket_object" "documents" {
  for_each     = var.objects
  bucket       = aws_s3_bucket.default.id
  key          = each.value.document_name
  source       = each.value.source
  content_type = each.value.content_type
  metadata     = {
    sha256 = filesha256(each.value.source)
  }
  # etag is not working on large assets
  etag = filemd5(each.value.source)

  tags = {
    env = var.environment
  }
}

resource "aws_s3_bucket_public_access_block" "default" {
  bucket = aws_s3_bucket.default.bucket

  block_public_acls       = true
  block_public_policy     = true
  restrict_public_buckets = true
  ignore_public_acls      = true
}

resource "aws_s3_bucket_policy" "default" {
  for_each = var.bucket_policies
  bucket   = aws_s3_bucket.default.id

  policy = each.value

  depends_on = [aws_s3_bucket_public_access_block.default]
}
