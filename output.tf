output "bucket_id" {
  value = aws_s3_bucket.default.id
}

output "bucket" {
  value = aws_s3_bucket.default.bucket
}

output "bucket_arn" {
  value = aws_s3_bucket.default.arn
}